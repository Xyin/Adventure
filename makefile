VERSION := 0.0.0
CXX := g++
WINC := i686-w64-mingw32-g++
CFLAGS := -g
WINLINKERFLAGS := -static-libgcc -static-libstdc++ -lm
FILENAME := adventure_$(VERSION)

$(FILENAME): obj/*.o 
	$(CXX) $(inputs) -o $(output) 

obj/%.o: src/%.cpp $(HEADERS)
	$(CXX) $(CFLAGS) -c $(input) -o $(output)

$(FILENAME).exe: obj/win/*.o 
	$(WINC) $(inputs) -o $(output) $(WINLINKERFLAGS)

obj/win/%.o: src/%.cpp $(HEADERS)
	$(WINC) $(CFLAGS) -c $(input) -o $(output)
	
