#ifndef LOCATION_H
#define LOCATION_H
#include <cstddef>
#include <cstring>
#include "object.h"

enum compass_t {NORTH, EAST, SOUTH, WEST, UP, DOWN, ERR=-1};

class Location: public Object{
	Location *north;
	Location *east;
	Location *south;
	Location *west;
	Location *up;
	Location *down;

	public:	
		Location(char const newName[20],char const newDesc[1024]);
		void connectTo(Location *n, Location *e, Location *s, Location *w, Location *u=NULL, Location *d=NULL);
		Location *mover(compass_t dir);
		
};

Location *init_Map(void);
#endif
