#include "location.h"
#include "initMap.h"

Location *initMap(void){
	//This code will link all of the map locations together when the program starts.
	Location *startPtr = new Location("Main Room","An average room. There's a door to the north");

	Location *kitchenPtr = new Location("Kitchen","An average kitchen. It's pretty neat. There's a door facing south and a very suspicious looking gateway that seems to lead to oblivion facing east.");

	Location *lairPtr = new Location("Dragon's Lair","A devilishly sinister dragon's lair. Inside is a dragon. He looks hungry.");

	startPtr->connectTo(kitchenPtr,NULL,NULL,NULL);
	kitchenPtr->connectTo(NULL,lairPtr,startPtr,NULL);
	lairPtr->connectTo(NULL,NULL,NULL,kitchenPtr);		
	
	return startPtr; //Starting Location
}
