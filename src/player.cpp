#include "player.h"
#include "location.h" 

int Player::Move(compass_t dir){
	Location *moveTo = cur_loc->mover(dir);
	if(!moveTo)
		return -1; //Got NULL pointer, i.e, moving that direction is impossible.
	else{
		cur_loc=moveTo;
		return 0;
	}
}
	
