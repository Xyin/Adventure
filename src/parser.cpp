#include "location.h"
#include "parser.h"

char *get_line (char *s, size_t n, FILE *f)
{
  char *p = fgets (s, n, f);

  if (p != NULL) {
    size_t last = strlen (s) - 1; //What on earth is a size_t

    if (s[last] == '\n') s[last] = '\0';
  }
  return p;
}

compass_t direction(char *input){
	switch(input){
		case 'n':
			return NORTH;
		case 'e':
			return EAST;
		case 's':
			return SOUTH;
		case 'w': 
			return WEST;
		default:
			return ERR;
	}
}
