#ifndef PARSER_H
#define PARSER_H

compass_t direction(char input);

char *get_line (char *s, size_t n, FILE *f);
#endif
