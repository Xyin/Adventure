#include "player.h"
#include "location.h"
#include "parser.h"
#include <cstddef>
#include <stdio.h>
#include "initMap.h"
int main(){
	Location *start;
	start = initMap();
	printf("Welcome to... uhm... the game thing. Feel free to move around, I guess. Just say n,e,s,w cause I'm lazy and anything else will probably break the game.\n");
	Player player;
	player.cur_loc = start;
	char input[1024]; // Sounds good.
	int stat;	
	compass_t dir;
	player.cur_loc->print();	
	printf("\n");
	
	while(1==1){	
		if(!fgets(input,1024,stdin)){printf("Something went horribly wrong.\n"),break;}
		if(input=='\n') continue;	
		dir = direction(input);
		int stat;
		if(dir==ERR)
			printf("That's not a valid direction!\n");
		else{
			stat = player.Move(dir);
			if(stat==-1)
				printf("You can't go that way.\n");
		}
	
		player.cur_loc->print();
		printf("\n");
	}
	return 0;
}	
		
