#include "location.h"
#include <stdlib.h>
#include <stdio.h>

Location::Location(char const newName[20], char const newDesc[1024]){
	north = NULL;
	east = NULL;
	south = NULL;
	west = NULL;
	up = NULL;
	down = NULL;	
	strcpy(name, newName);
	strcpy(desc,newDesc);
}

void Location::connectTo(Location *n, Location *e, Location *s, Location *w, Location *u, Location *d){
	//TODO: Make linking rooms together easier?
	north = n;
	east = e;
	south = s;
	west = w;
	up = u;
	down = d;
}

Location *Location::mover(compass_t dir){
	switch(dir){
		case NORTH:
			return north;
		case EAST:
			return east;
		case SOUTH:
			return south;
		case WEST:
			return west;
		case UP:
			return up;
		case DOWN:
			return down;
		default: //Should never happen but lets double check
			return NULL;
	}

}

